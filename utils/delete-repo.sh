#!/usr/bin/env sh

repo="$1"
token="$QUAY_APP_TOKEN"

curl --fail --silent -X "DELETE" \
  "https://quay.rdnxk.com/api/v1/repository/$repo" \
  -H "Authorization: Bearer $token" \
  -H "Accept: application/json"
