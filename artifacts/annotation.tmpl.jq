{
    ($input_file): {
        "org.opencontainers.image.title": $output_file,
        "org.opencontainers.image.created": $date,
        "org.opencontainers.image.description": $file_description
    },
    "$config": {
        "org.opencontainers.image.created": $date
    },
    "$manifest": {
        "org.opencontainers.image.title": "Podman Steroids",
        "org.opencontainers.image.description": "Podman, but bundled with \"steroids\", for building and auditing containers, and managing repositories",
        "org.opencontainers.image.vendor": "rdnxk",
        "org.opencontainers.image.authors": "rdnxk, San 'rdn' Mônico <san@monico.com.br>",
        "org.opencontainers.image.url": "https://hub.docker.com/r/redemonbr/podman-steroids",
        "org.opencontainers.image.source": "https://gitlab.com/rdnxk/podman-steroids",
        "org.opencontainers.image.documentation": "https://gitlab.com/rdnxk/podman-steroids/-/blob/master/README.md",
        "org.opencontainers.image.created": $date
    }
}
