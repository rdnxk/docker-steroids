# Podman Steroids

Podman, but bundled with "steroids", for building and auditing containers, and managing repositories


## What is this image
This image contains tools for building, auditing and publishing OCI and Docker images and containers in small image (\<180MB with Alpine variants).
It contains:
-   [**Podman**](https://podman.io) Container/Pod Manager
-   [**Qemu**](https://www.qemu.org) machine emulator/virtualizer for cross-platform container builds
-   [**crane**](https://github.com/google/go-containerregistry) tool for interacting with remote images and registries
-   [**Cosign**](https://github.com/sigstore/cosign) container signing tool
-   [**Grype**](https://github.com/anchore/grype) container vulnerability scanner
-   [**ORAS**](https://oras.land) OCI Artifact manager
-   [**Skopeo**](https://github.com/containers/skopeo) image repository management CLI utilities
-   [**Syft**](https://github.com/anchore/syft) tool for generating SBOM (Software Bill of Materials)


## Base images
At the time of this writing, only [Alpine Linux](https://hub.docker.com/_/alpine) is available as the base underlying container system. But more are planned due to `glib` tools

### Default variants (`<version>`)
These are the defacto images. They are built on top of the latest stable version of Alpine Linux, and all the architecture emulators from Qemu are installed.


### Minimal variants (`<version>-minimal`)
The minimal variants have limited foreign architecture emulation support, supporting only ARM (`arm/v7`, `arm64/v8` - little endian variant) and x86 (`386` and `amd64`) architectures for the sake of smaller pull sizes (\<150 MB) compared to the default variant.  
These are the most popular architectures and the built images will be able to be executed in most machines, including Apple MacOS devices (Apple Silicon or Intel CPUs), Linux with x86 CPU (AMD/Intel) or ARM v7 (like Raspberry Pi 2 or higher), and [WSL - Windows Subsystem for Linux](https://learn.microsoft.com/en-us/windows/wsl/about).

> There may be some scenarios where these images will be able to build for other architectures, such like `arm/v5` or `arm/v6` (due to some compatibility with `arm/v7`), but they are not supported.  
> For a larger list of architectures, use the default variant which has all Qemu emulators available

_In the future, we may remove support for 32-bit architectures (i.e. `arm/v7` and `386`) for even smaller pulls._


### Edge variants (`<version>-edge`)
The Alpine "Edge" variants include the latest version of Podman and friends available in Alpine Edge repository, but the upstream compatibility is not guaranteed.  
Use it with caution in development environments to know what is coming next from upstream packages. But avoid usage in production environments for stability purpose.  
For more information on Alpine Edge, check [Repositories page in their Wiki](https://wiki.alpinelinux.org/wiki/Repositories#Edge)


## Source of these images
-   Repository: <https://gitlab.com/rdnxk/podman-steroids>
-   Issues: <https://gitlab.com/rdnxk/podman-steroids/-/issues>
-   Containerfile: <https://gitlab.com/rdnxk/podman-steroids/-/blob/master/alpine/Containerfile>

These images are automatically updated on a weekly basis.
Attestations, SBOM and Signatures are attached to the image digests:
-   **Attestation**: Attestation predicate is attached with [CycloneDX](https://cyclonedx.org/specification/overview/) in JSON (`application/cyclonedx+json`) format. CycloneDX is a modern standard for the software supply chain
-   **SBOM**: SBOM is attached with [Syft](https://github.com/anchore/syft) in JSON (`application/vnd.syft+json`) format. Syft SBOMs are good for using in conjunction with [Grype](https://github.com/anchore/grype) for vulnerability scanning

_NOTE: Until 2023-08-13, the attestations were in [SPDX](https://spdx.dev) in JSON (`application/spdx+json`) format._

### OCI Artifacts
Other OCI artifacts are pushed to this repository. They include the License, Public Key and README with the following tags
-   **license**: Refers to the latest license
-   **license_&lt;DATE&gt;**: Refers to the license that was pushed at specified `DATE` datetime. Useful if the latest license was updated
-   **publickey**: Refers to the latest public key
-   **publickey_&lt;DATE&gt;**: Refers to the public key that was pushed at specified `DATE` datetime. Useful when the keys used to sign the old image and artificats got rotated
-   **readme**: Refers to the lastest README
-   **readme_&lt;DATE&gt;**: Refers to the README that was pushed at the specified `DATE` datetime. Useful if the latest README was updated.

The public keys can be used to validate the authenticity of the images and artifacts. They can be used as below
```bash
## Display signatures and artifacts related to image
$ cosign triangulate docker.io/redemonbr/podman-steroids:latest
$ cosign tree docker.io/redemonbr/podman-steroids:latest

## Pulls/Downloads the public key (it will be saved as cosign.pub)
$ oras pull docker.io/redemonbr/podman-steroids:publickey
## Verify image (via manifest/tag or digest), SBOM and attestation
$ cosign verify --key cosign.pub docker.io/redemonbr/podman-steroids:latest
$ cosign verify --key cosign.pub docker.io/redemonbr/podman-steroids@sha256:...
$ cosign verify --attachment sbom --key cosign.pub docker.io/redemonbr/podman-steroids@sha256:...
$ cosign verify-attestation --type cyclonedx --key cosign.pub docker.io/redemonbr/podman-steroids@sha256:...
```

## Environment Variables Suppported
All environment variables inherited from bundled packages and tools are respected, like `BUILDAH_FORMAT`, `STORAGE_DRIVER` `CONTAINER_SSHKEY` etc.

This image implements only one environment variable to customize its behavior:
-   **BINFMT\_CONF\_QUIET**: If `0` or `false`, it will not suppress the `qemu-binfmt-conf.sh` output on entrypoint. Output is suppressed by default. Default value is `1` which supresses the output


## Using this image
### With `--privileged` for running Podman inside a Podman container
For running with privileged access. Using podman as example, but also [works with Docker engine too](#with---privileged-for-running-podman-in-docker)
```console
###### IN HOST MACHINE ######
$ podman run --privileged --rm --name podman-steroids -it docker.io/redemonbr/podman-steroids:latest /bin/bash

###### INSIDE CONTAINER ######
## run command inside another Ubuntu container
$ podman run --rm --name ubuntu docker.io/ubuntu:latest "echo hello world from ubuntu container"
## generate SBOM from Ubuntu image
$ syft packages --output syft-json --file syft-sbom.json registry:docker.io/ubuntu:latest
## scan SBOM from Ubuntu SBOM
$ grype sbom:./syft-sbom.json
```

### With `--privileged` for running Podman in Docker
In host machine, with Docker installed:
```console
# docker run --privileged --rm --name podman-steroids redemonbr/podman-steroids:latest podman version
```

### Continuous Integration example on GitLab-CI
Using GitLab Container Registry as example.

Also, for the examples including signature, it is required to have a `cosign` pair of keys ([how to generate](https://github.com/sigstore/cosign/blob/main/README.md#generate-a-keypair)) stored in GitLab CI Secrets:
-   **COSIGN\_PRIVATE\_KEY** with type `File` with the contents of the private key
-   **COSIGN\_PUBLIC\_KEY** with type `File` with the contents of the public key
-   **COSIGN\_PASSWORD** with the type `Variable` with the password. [_Remember to mask it_](https://gitlab.com/help/ci/variables/index#mask-a-cicd-variable)

Also, some variables are predefined by GitLab CI, including `CI_REGISTRY`, `CI_REGISTRY_USER`, `CI_REGISTRY_PASSWORD`, and `CI_PROJECT_PATH`. More information on [GitLab CI's Predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)

#### Basic usage example
Simple build and push
```yml
build:
  stage: build
  image: redemonbr/podman-steroids:latest
  variables:
    IMAGE_REPO: $CI_REGISTRY/$CI_PROJECT_PATH/my-image
    TAG: example
  script:
    - podman login --username $CI_REGISTRY_USER --password $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - podman build --tag $IMAGE_REPO:$TAG .
    - podman push $IMAGE_REPO:$TAG
```

#### Attaching signature, SBOM and attestation
Build an image, sign the image, generate and attach SBOM (Software Bill of Materials) and attestation to the image in remote repository.

_NOTE: This considers `COSIGN_PRIVATE_KEY` and `COSIGN_PUBLIC_KEY` are set in GitLab CI secrets_
```yml
build:
  stage: build
  image: redemonbr/podman-steroids:latest
  variables:
    IMAGE_REPO: $CI_REGISTRY/$CI_PROJECT_PATH/my-image
    TAG: example
  before_script:
    - podman login --username $CI_REGISTRY_USER --password $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - cosign login --username $CI_REGISTRY_USER --password $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - podman build --squash --jobs $(nproc) --tag $IMAGE_REPO:$TAG.
    - podman push $IMAGE_REPO:$TAG
    ## Sign the image and push signature, and then verify
    - cosign sign --yes --key $COSIGN_PRIVATE_KEY $IMAGE_REPO:$TAG
    - cosign verify --key $COSIGN_PUBLIC_KEY $IMAGE_REPO:$TAG
    ## Generate SBOM, attach it to remote container registry, sign and then verify
    - syft packages --output syft-json --file syft-sbom.json $IMAGE_REPO:$TAG
    - cosign attach sbom --type syft --sbom syft-sbom.json $IMAGE_REPO:$TAG
    - cosign sign --yes --attachment sbom --key $COSIGN_PRIVATE_KEY $IMAGE_REPO:$TAG
    - cosign verify --attachment sbom --key $COSIGN_PUBLIC_KEY $IMAGE_REPO:$TAG
    ## Scan the SBOM for vulnerabilities
    - grype sbom:./syft-sbom.json
    ## Generate attestation, attach it to remote container registry and then verify it
    - syft packages --output cyclonedx-json --file cyclonedx-sbom.json $IMAGE_REPO:$TAG
    - cosign attest --yes --type cyclonedx --predicate cyclonedx-sbom.json --key $COSIGN_PRIVATE_KEY $IMAGE_REPO:$TAG
    - cosign verify-attestation --type cyclonedx --key $COSIGN_PUBLIC_KEY $IMAGE_REPO:$TAG
```

For more information, check our own [repository](https://gitlab.com/rdnxk/podman-steroids) that generates this very own image


#### GitLab-CI multi-arch image build
Build a multi-arch image using [manifests](https://docs.podman.io/en/latest/markdown/podman-manifest.1.html) (instead of tags) and pushing it to remote Container Registry.
```yml
build:
  stage: build
  image: redemonbr/podman-steroids:latest
  variables:
    IMAGE_REPO: docker.io/namespace/my-image
    REGISTRY_USER: my-user
    REGISTRY_PASSWORD: password-safely-stored-in-secret-manager
    MANIFEST_NAME: example
    PLATFORMS: linux/amd64,linux/i386,linux/arm64/v8,linux/arm/v7
  before_script:
    - podman login --username $REGISTRY_USER --password $REGISTRY_PASSWORD docker.io
    - cosign login --username $REGISTRY_USER --password $REGISTRY_PASSWORD docker.io
  script:
    - podman build --squash --jobs $(nproc) --platform $PLATFORMS --manifest $IMAGE_REPO:$MANIFEST_NAME .
    - podman manifest push $IMAGE_REPO:$MANIFEST_NAME $IMAGE_REPO:$MANIFEST_NAME
    ## signing recursively all platforms included in manifest list
    - cosign sign --yes --key $COSIGN_PRIVATE_KEY --recursive $IMAGE_REPO:$MANIFEST_NAME
```

For more usage with GitLab CI, please check the [source code of this own project](https://gitlab.com/rdnxk/podman-steroids) and to see how it uses GitLab CI for automated builds

## Caveats

### Emulating other architectures
Podman uses [Buildah](https://buildah.io) for building container images.
While _Buildah_ and _Podman_ are happy to use base images and `COPY`/`ADD`-ing resources from foreign platforms during builds, the `RUN` instructions require emulation provided by QEMU.
For the `RUN` instructions to be emulated, QEMU relies on hypervisors (aka accelerators), such like **[KVM](https://en.wikipedia.org/wiki/Kernel-based_Virtual_Machine)**, from the host machine.
If the host machine does not supported them, the `RUN` instructions will potentially fail for foreign platforms.
Even without KVM, QEMU is, under certain circumstances (like newer versions of Linux Kernel, high-spec hosts etc), able to emulate foreign platforms but with a lower performance. For more information about QEMU accelerators support, read [QEMU documentation](https://www.qemu.org/docs/master/system/introduction.html).

Most of cloud Virtual Private Server providers do not provide virtual machines with KVM support. So if you are using custom CI runners on the cloud or using this in a virtual machine, please be sure to [check if the host machine has KVM support](https://wiki.archlinux.org/title/KVM#Hardware_support).

For more information:
-   [Podman Build docs](https://docs.podman.io/en/latest/markdown/podman-build.1.html#platform-os-arch-variant)


## How to allow Docker to emulate a foreign architecture
This project hosts a living document of how to make Docker emulate foreign architectures, for running and building containers. The rdnxk's custom GitLab CI runners use Docker, similar to the one used in the tutorial.

Check the [multi-arch docker tutorial](https://gitlab.com/rdnxk/podman-steroids/-/blob/main/multi-arch-docker.md)

## TODO
These are the plans for the upcoming releases. We don't promise anything but we want to track it when we have some time:
-   Rootless mode support
-   Fedora builds, for `glib` and friends
-   More multi-arch tutorials, now including Podman
