# REQUIRED VARIABLES
BUILDER ?= podman
BUILD_PLATFORM ?= linux/amd64,linux/arm64/v8
BUILD_CONTEXT ?= ./alpine
LOCAL_IMAGE_NAME ?= local:temp
TAG_SUFFIX ?= alpine
ALPINE_VERSION ?= latest
REMOTE_IMAGE_REPO ?= localhost:5000/rdnxk/podman-steroids
REMOTE_IMAGE_NAME ?= $(REMOTE_IMAGE_REPO):$(TAG_SUFFIX)
COSIGN_PRIVATE_KEY ?= cosign.key
COSIGN_PUBLIC_KEY ?= cosign.pub
COSIGN_PASSWORD ?= foobar

# OPTIONAL VARIABLES
BUILD_OPTS ?= --squash --jobs 2
PUSH_OPTS ?= --tls-verify=false
INNER_CONTAINER_ENV ?= BUILDAH_ISOLATION=chroot
## THE "__" PREFIXED ARE NOT **DIRECTLY** USED IN THE MAKEFILE
__INNER_CAPABILITIES ?= SYS_ADMIN,SYS_CHROOT,MKNOD,NET_ADMIN,NET_RAW
__INNER_SECURITY_OPTS ?= --security-opt apparmor=unconfined --security-opt seccomp=unconfined --security-opt label=disable --security-opt unmask=/sys/fs/cgroup:/proc/sys
INNER_CONTAINER_RUN_OPTS ?= --cap-add ${__INNER_CAPABILITIES} ${__INNER_SECURITY_OPTS} --device /dev/fuse --cgroups=disabled

## Use 0 or false to disable. Enabled otherwise
## If you are using a local non-HTTPS registry, set INSECURE_REGISTRY to 1 or true
INSECURE_REGISTRY ?= false
