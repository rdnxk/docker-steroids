# Cosign and Crane include non-stable pre-release versions in 'latest' tag,
# Oras do not have a 'latest' tag,
# So a script gets their latest stable versions and passes them here as build arguments
ARG COSIGN_VERSION=v2.2.3
ARG ORAS_VERSION=v1.1.0
ARG ALPINE_VERSION=latest
ARG VARIANT=default

FROM gcr.io/projectsigstore/cosign:$COSIGN_VERSION AS cosign
FROM docker.io/anchore/grype:latest AS grype
FROM ghcr.io/oras-project/oras:$ORAS_VERSION AS oras
# FROM quay.io/skopeo/stable:latest AS skopeo
FROM docker.io/anchore/syft:latest AS syft

FROM docker.io/library/alpine:$ALPINE_VERSION
ARG QEMU_BINFMT_FILE="qemu-binfmt-conf.sh"
ARG TARGETPLATFORM TARGETOS TARGETARCH TARGETVARIANT ALPINE_VERSION VARIANT

LABEL \
    # OCI labels
    org.opencontainers.image.authors="rdnxk (https://rdnxk.com), San 'rdn' Mônico <san@rdnxk.com> (https://rdnxk.com)" \
    org.opencontainers.image.description="Podman, but with steroids for building and auditing containers" \
    org.opencontainers.image.documentation="https://gitlab.com/rdnxk/podman-steroids/-/blob/main/README.md" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.source="https://gitlab.com/rdnxk/podman-steroids" \
    org.opencontainers.image.title="podman-steroids" \
    org.opencontainers.image.url="https://gitlab.com/rdnxk/podman-steroids" \
    org.opencontainers.image.vendor="rdnxk" \
    # Custom labels
    platform="$TARGETPLATFORM" \
    os="$TARGETOS" \
    arch="$TARGETARCH" \
    variant="$TARGETVARIANT" \
    maintainer="san@monico.com.br"

COPY --from=cosign /ko-app/cosign /usr/bin/cosign
COPY --from=grype /grype /usr/bin/grype
COPY --from=oras /bin/oras /usr/bin/oras
# COPY --from=skopeo /usr/bin/skopeo /usr/bin/skopeo
COPY --from=syft /syft /usr/bin/syft
COPY entrypoint.sh /usr/local/bin/entrypoint.sh

RUN apk upgrade --no-cache --update && \
    set -eux && \
    if [ "$VARIANT" = "minimal" ] ; then \
        # archs
        qemu_archs="qemu-aarch64 qemu-arm qemu-i386 qemu-x86_64" ; \
        # qemu tools
        qemu_packages="$qemu_archs qemu-bridge-helper qemu-pr-helper qemu-tools" ; \
    else \
        # list all packages that does not include these ones mentioned below
        qemu_packages=$(apk search --no-cache -xq qemu-* | grep -Ev "(-audio-|-block-|-hw-|-system-|-ui-|-vhost-)" | grep -Ev "(-chardev-spice|-doc|-guest-agent|-img|-lang|-modules|-openrc|virtiofsd)$") ; \
        qemu_packages=$(echo "$qemu_packages" | tr '\n' ' ') ; \
    fi ; \
    set +eux && \
    echo "$qemu_packages" | xargs apk add --no-cache --virtual .qemu && \
    apk add --no-cache bash coreutils curl jq make && \
    apk add --no-cache crane fuse-overlayfs podman podman-docker skopeo && \
    qemu_version="$(apk list --installed qemu-tools | cut -d- -f3)" && \
    echo "qemu_version=$qemu_version" && \
    wget -qO "/usr/bin/$QEMU_BINFMT_FILE" "https://raw.githubusercontent.com/qemu/qemu/v${qemu_version}/scripts/$QEMU_BINFMT_FILE" && \
    chmod +x "/usr/bin/$QEMU_BINFMT_FILE" && \
    chmod +x /usr/local/bin/entrypoint.sh && \
    echo "apk --print-arch --> $(apk --print-arch)" && \
    cosign version && \
    crane version && \
    grype version && \
    oras version && \
    skopeo --version && \
    syft version && \
    podman -v

ENV BINFMT_CONF_QUIET=1

ENTRYPOINT ["entrypoint.sh"]

CMD ["podman", "info"]
