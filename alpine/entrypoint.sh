#!/usr/bin/env sh

if [ -n "$BINFMT_CONF_QUIET" ] && [ "$BINFMT_CONF_QUIET" -ne 0 ] && [ "$BINFMT_CONF_QUIET" != "false" ]; then
    qemu-binfmt-conf.sh > /dev/null 2>&1
else
    qemu-binfmt-conf.sh
fi

exec "$@"
