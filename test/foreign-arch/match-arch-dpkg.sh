#!/usr/bin/env sh

## Inputs (optional), TARGETPLATFORM (ex: 'linux/amd64' | 'linux/arm64/v8' | 'linux/arm/v7' )
docker_arch="${1:-$TARGETPLATFORM}"
docker_arch="${docker_arch#*/}"

dpkg_arch="$(dpkg --print-architecture)"

## convert dpkg arch to docker arch
case "$dpkg_arch" in
    arm64)
        arch=arm64/v8 ;;
    armel)
        ## armel also is the dpkg arch of linux/arm/v5'
        arch=arm/v5 ;;
    armhf)
        arch=arm/v7 ;;
    i386)
        arch=386 ;;
    mips64el)
        arch=mips64le ;;
    ppc64el)
        arch=ppc64le ;;
    *)
        arch="$dpkg_arch" ;;
esac

[ -z "$arch" ] && echo "undetected or unsupported arch" && exit 1

if [ "$arch" != "$docker_arch" ]; then
    echo "Architectures do not match: ARCH=$dpkg_arch ; TARGET=$docker_arch"
    exit 1
else
    echo "Architectures match: ARCH=$dpkg_arch ; TARGET=$docker_arch"
fi
